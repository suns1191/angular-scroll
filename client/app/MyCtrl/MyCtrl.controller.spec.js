'use strict';

describe('Controller: MyCtrlCtrl', function () {

  // load the controller's module
  beforeEach(module('trngleSagarApp'));

  var MyCtrlCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MyCtrlCtrl = $controller('MyCtrlCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
